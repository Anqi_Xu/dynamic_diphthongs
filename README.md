### The project contains the datasets for:
### Anqi Xu, Daniel van Niekerk, Branislav Gerazov, Paul Konstantin Krug, Santitham Prom-on, Peter Birkholz, Yi Xu. Learnability of English diphthongs: One dynamic target vs. two static targets.

### [Demonstration video](https://gitlab.com/Anqi_Xu/dynamic_diphthongs/-/blob/main/diphthong_full_illustration.mp4) illustrates the structure of the vocal learning model and shows the learned articulatory movements of the 3D vocal tract models. 

### [Stimuli](https://gitlab.com/Anqi_Xu/dynamic_diphthongs/-/tree/main/Stimuli) contains the synthetic samples used in the perception experiments.

### [Stats](https://gitlab.com/Anqi_Xu/dynamic_diphthongs/-/blob/main/R_scripts_for_stats.R) contains the R code for running the statistical analysis.

### [Code](https://gitlab.com/Anqi_Xu/dynamic_diphthongs/-/tree/main/Code) contains the Python code for running the simulation. It will be available upon publication.
